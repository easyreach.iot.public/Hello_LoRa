#include"stdio.h"
#include"string.h"
#include"Nano100Series.h"

#define ABP 													0
#define OTAA 													1
#define JOIN_METHOD										OTAA
#define EU_868												868
#define SENRA_TCL											865
#define LORA_TX_PORT									5
#define LORA_BAND											SENRA_TCL
#define WAIT_FOR_JOIN 								JOIN_METHOD
#define WAIT_FOR_ACK 									JOIN_METHOD

#define DEBUG_LORA										1

/**
* This is a key variable. When you get board in hand for the first time, you need to configure LoRa parameters.
* Make INIT_SYSTEM to 1 so that  the board will be configured for LoRa. 
*
* Note: This is one time activity. Change it back to 0 and flash the firmware.
*/
#define INIT_SYSTEM 									0

#define STATE_POWER_DOWN							1
#define STATE_POWER_UP								2

#if JOIN_METHOD == ABP

#define DEVICE_ADDRESS 								3
#define DEVICE_ADDRESS_COMMAND 				"mac set_devaddr 00000003"
#define APPSKEY_COMMAND 							"mac set_appskey FFEEDDCCBBAA99887766554433221100"
#define NWKSKEY_COMMAND 							"mac set_nwkskey 00112233445566778899AABBCCDDEEFF"

#elif JOIN_METHOD == OTAA

#define DEVEUI_COMMAND 								"mac set_deveui 0102030405060708"
#define APPEUI_COMMAND 								"mac set_appeui ECFAF4FE00000001"
#define APPKEY_COMMAND 								"mac set_appkey C93567C21147FDBCCF0D9A1B06FE0AE5"

#endif

int timer0_flag=0;
int uart1_flag=0;
char transmission_data[100];

unsigned char led_status=0;

volatile unsigned char queue[50]={0};
int tail = 0;

extern int IsDebugFifoEmpty(void);

#if WAIT_FOR_JOIN == 1
	
int wait_for_join(void)
{
	int failure_threshhold = 0;
	while(failure_threshhold < 100)
	{
#if DEBUG_LORA == 1		
		printf("Queue is now:\n %s\n length %d\n",queue,strlen((char*)queue));
#endif
		
		if(strstr((char*)queue,"accepted"))
		{
			tail = 0;
			memset((char *)queue,0,sizeof(queue));
			return 1;
		}
		
		TIMER_Delay(TIMER1,1000000);
		
		failure_threshhold++;
	}
	
	return 0;
}

#endif

#if WAIT_FOR_ACK == 1
	
int check_if_acked(void)
{
	while(1)
	{
#if DEBUG_LORA == 1		
		printf("Queue is now:\n %s\n length %d\n",queue,strlen((char*)queue));
#endif

		if(strstr((char*)queue,"err"))
		{
			tail = 0;
			memset((char *)queue,0,sizeof(queue));
			return 0;
		}
		else if(strstr((char*)queue,"tx_ok"))
			return 1;
		TIMER_Delay(TIMER1,1000000);
	}
}	

#endif

void uart_init(void)
{
	
#if 1
		SYS_UnlockReg();
	
		SYS_LockReg();
#endif
	
		UART_Open(UART0,115200);
		UART_Open(UART1,115200);
		
		TIMER_Delay(TIMER1,250000);
		
		UART_EnableInt(UART1,UART_IER_RDA_IE_Msk);
		NVIC_EnableIRQ(UART1_IRQn);

}

void uart_destroy(void)
{
	NVIC_DisableIRQ(UART1_IRQn);
	UART_DisableInt(UART1,UART_IER_RDA_IE_Msk);
	
	UART_Close(UART0);
	UART_Close(UART1);

#if 1	
	SYS_UnlockReg();

	CLK_DisableModuleClock(UART0_MODULE);
	CLK_DisableModuleClock(UART1_MODULE);
	
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB0_MFP_Msk | SYS_PB_L_MFP_PB1_MFP_Msk);
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB4_MFP_Msk | SYS_PB_L_MFP_PB5_MFP_Msk);
	
	SYS_LockReg();
#endif
	
	TIMER_Delay(TIMER1,250000);
}


#if INIT_SYSTEM == 1
/**
* For different Network you will need to change the network key accordingly.
*	"mac set_nwkskey 00112233445566778899aabbccddeeff"
*	"mac set_appskey ffeeddccbbaa99887766554433221100"
* Note:  This will work only on ABP activation. OTAA will need different set of keys e.g. APPEUI,DEVEUI,APPKEY .
*/
char *commands[]=
		{
			"sip factory_reset",
			"sip reset",
			"sip set_log debug",
#if LORA_BAND	==	SENRA_TCL			
			"mac set_ch_freq 0 865062500",
			"mac set_ch_freq 1 865402500",
			"mac set_ch_freq 2 865985000",
			"mac set_ch_freq 3 865602500",
			"mac set_ch_freq 4 865785000",
			"mac set_ch_freq 5 866300000",
			"mac set_ch_freq 6 866500000",
			"mac set_ch_freq 7 866700000",
			"mac set_rx2 2 866550000",
#elif LORA_BAND	==	EU_868			
			"mac set_ch_freq 0 868100000",
			"mac set_ch_freq 1 868300000",
			"mac set_ch_freq 2 868500000",
			"mac set_ch_freq 3 867100000",
			"mac set_ch_freq 4 867300000",
			"mac set_ch_freq 5 867500000",
			"mac set_ch_freq 6 867700000",
			"mac set_ch_freq 7 867900000",
			"mac set_rx2 2 869525000",
#endif			
			"mac set_sync 12",
			"mac set_ch_status 0 on",
			"mac set_ch_status 1 on",
			"mac set_ch_status 2 on",
			"mac set_ch_status 3 on",
			"mac set_ch_status 4 on",
			"mac set_ch_status 5 on",
			"mac set_ch_status 6 on",
			"mac set_ch_status 7 on",
			"mac set_ch_status 8 on",
			"mac set_ch_status 9 on",
			"mac set_ch_status 10 on",
			"mac set_ch_status 11 on",
			"mac set_ch_status 12 on",
			"mac set_ch_status 13 on",
			"mac set_ch_status 14 on",
			"mac set_ch_status 15 on",
			"rf set_sf 9",
			"rf set_sync 12",
			"rf set_bw 125",
			"rf set_cr 4/5",
			"rf set_pwr 20",
			"mac set_rxdelay1 1000",
			"mac set_class A",
#if JOIN_METHOD == OTAA
			DEVEUI_COMMAND,
			APPEUI_COMMAND,
			APPKEY_COMMAND,
#elif JOIN_METHOD == ABP
			DEVICE_ADDRESS_COMMAND,
			APPSKEY_COMMAND,
			NWKSKEY_COMMAND,
#endif			
			"mac set_join_ch 0 on",
			"mac set_join_ch 1 on",
			"mac set_join_ch 2 on",
			"mac set_adr on",
			"mac set_dr 0",
			"mac save",
			"rf save",
#if 0
#if JOIN_METHOD == OTAA
			"mac join otaa",
#elif JOIN_METHOD == ABP
			"mac join abp",
#endif
			"mac tx cnf 15 ababab"
#endif
		};
#endif

/**
* Don't forget to change this number if you happen to add / remove command from <b>commands</b> data structure.
*/		
int number_of_commands=46;		

int tap_status=0;	

int check_timer0_status(void)
{
	return timer0_flag;
}

int check_uart1_status(void)
{
	return uart1_flag;
}

void shift_queue(unsigned char data)
{
	int i;
	
	if(data == '\r' || data == '\n')
		return;
	
	if(tail == sizeof(queue) - 1)
	{
		for(i=1;i<tail;i++)
		{
			queue[i-1] = queue[i];
		}
		tail--;
	}
	
	queue[tail++] = data;
}

void UART1_IRQHandler(void)
{
//	UART_WRITE(UART0,UART_READ(UART1));
	UART_ClearIntFlag(UART1,UART_IER_RDA_IE_Msk);
#if INIT_SYSTEM == 0	
	shift_queue(UART_READ(UART1));
#else
	UART_WRITE(UART0,UART_READ(UART1));
#endif	
}

/*
*	Enable following if UART0 commands are to be transferred to S76S (UART1).
*/
#if 0
void UART0_IRQHandler(void)
{
	UART_WRITE(UART1,UART_READ(UART0));
}
#endif

/*
*	This suggests that timer has expired and data is to be sent over LoRa.
*/
void TMR0_IRQHandler(void)
{
	TIMER_ClearIntFlag(TIMER0);
	TIMER_ClearWakeupFlag(TIMER0);
	if(!timer0_flag)
	{
		timer0_flag=1;
	}
}

void TMR2_IRQHandler(void)
{
	TIMER_ClearIntFlag(TIMER2);
	TIMER_ClearWakeupFlag(TIMER2);
	led_status=!led_status;
	PA3=led_status;
}

/*
*	This function will initialize the system clocks and GPIO multiplexed functions so that system can comunicate over UART0, UART1.
*/
void init(void)
{
	SYS_UnlockReg();
	
	CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC,CLK_HCLK_CLK_DIVIDER(1));
	
	CLK_EnableXtalRC(CLK_PWRCTL_HIRC_EN);
	CLK_WaitClockReady(CLK_CLKSTATUS_HIRC_STB_Msk);
	
	CLK_EnableXtalRC(CLK_PWRCTL_LIRC_EN);
	CLK_WaitClockReady(CLK_CLKSTATUS_LIRC_STB_Msk);
	
	CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC,CLK_HCLK_CLK_DIVIDER(1));

	CLK_SetModuleClock(UART0_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_EnableModuleClock(UART0_MODULE);

	CLK_SetModuleClock(UART1_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_EnableModuleClock(UART1_MODULE);

	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB0_MFP_Msk | SYS_PB_L_MFP_PB1_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB1_MFP_UART0_TX | SYS_PB_L_MFP_PB0_MFP_UART0_RX);

	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB4_MFP_Msk | SYS_PB_L_MFP_PB5_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB4_MFP_UART1_RX | SYS_PB_L_MFP_PB5_MFP_UART1_TX);
	
	CLK_SetModuleClock(UART0_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_SetModuleClock(UART1_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_SetModuleClock(TMR0_MODULE,CLK_CLKSEL1_TMR0_S_LIRC,0);
	CLK_SetModuleClock(TMR1_MODULE,CLK_CLKSEL1_TMR1_S_HIRC,0);
	CLK_SetModuleClock(TMR2_MODULE,CLK_CLKSEL2_TMR2_S_LIRC,0);
	
	CLK_EnableModuleClock(UART0_MODULE);
	CLK_EnableModuleClock(UART1_MODULE);
	CLK_EnableModuleClock(TMR0_MODULE);
	CLK_EnableModuleClock(TMR1_MODULE);
	CLK_EnableModuleClock(TMR2_MODULE);
	
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB0_MFP_Msk | SYS_PB_L_MFP_PB1_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB1_MFP_UART0_TX | SYS_PB_L_MFP_PB0_MFP_UART0_RX);
	
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB5_MFP_Msk | SYS_PB_L_MFP_PB4_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB5_MFP_UART1_TX | SYS_PB_L_MFP_PB4_MFP_UART1_RX);
	
	SYS_LockReg();
}

void reset_modules(void)
{
	SYS_ResetModule(UART0_RST);
	SYS_ResetModule(UART1_RST);
	SYS_ResetModule(TMR0_RST);
	SYS_ResetModule(TMR1_RST);
	SYS_ResetModule(TMR2_RST);
	SYS_ResetModule(GPIO_RST);
}
/*
*	This is the main function .
*/
int main(void)
{
	int i,j,err_count = 0;
	
	init();
	
	reset_modules();
	
	GPIO_SetMode(PA,BIT3,GPIO_PMD_OUTPUT);
	GPIO_SetMode(PA,BIT4,GPIO_PMD_OUTPUT);
	GPIO_SetMode(PF, BIT1, GPIO_PMD_INPUT);
	
	GPIO_ENABLE_PULL_UP(PA,BIT4);
	PA3=0;
	
	PA4=1;
		
	UART_Open(UART0,115200);
	UART_Open(UART1,115200);
	
	UART_EnableInt(UART1,UART_IER_RDA_IE_Msk);
	NVIC_EnableIRQ(UART1_IRQn);
	
	TIMER_Delay(TIMER1,1000000);
	TIMER_Delay(TIMER1,500000);

#if INIT_SYSTEM == 1
	for(i=0;i<number_of_commands;i++)
	{
		printf("%s\n",commands[i]);
		UART_Write(UART1,(uint8_t *)commands[i],strlen(commands[i]));
		TIMER_Delay(TIMER1,1000000);
		TIMER_Delay(TIMER1,500000);
	}
	while(IsDebugFifoEmpty()==0);
	while(1);
#endif
	
	TIMER_Open(TIMER0,TIMER_PERIODIC_MODE,1);
	TIMER0->PRECNT = 179;
	TIMER0->CMPR = 200000;
	TIMER_EnableInt(TIMER0);
	TIMER_EnableWakeup(TIMER0);
	NVIC_EnableIRQ(TMR0_IRQn);
	
	while(!TIMER_IS_ACTIVE(TIMER0))
		TIMER_Start(TIMER0);

	TIMER_Open(TIMER2,TIMER_PERIODIC_MODE,2);
	TIMER2->CMPR = TIMER_GetModuleClock(TIMER2)/4;
	TIMER2->PRECNT = 0;
	TIMER_EnableInt(TIMER2);
	TIMER_EnableWakeup(TIMER2);
	NVIC_EnableIRQ(TMR2_IRQn);
		
	while(1)
	{
		PA4 = 0;
		TIMER_Delay(TIMER1,1000000);
		PA3=1;
		TIMER_Stop(TIMER2);
		TIMER2->CMPR = TIMER_GetModuleClock(TIMER2)/4;
		TIMER2->PRECNT = 0;
		PA3 = 1;
		printf("Trying to power down\n");
		while(IsDebugFifoEmpty() == 0);
		SYS_UnlockReg();
		CLK_PowerDown();
		SYS_LockReg();

		if(check_timer0_status())
		{
			printf("System has been interrupted!\n");
			PA3=0;
			printf("Turned on LED\n");
			while(!TIMER_IS_ACTIVE(TIMER2))
				TIMER_Start(TIMER2);
			PA4=1;
			TIMER_Delay(TIMER1,1000000);

#if JOIN_METHOD == OTAA
			printf("mac join otaa\n");
			UART_Write(UART1,(unsigned char *)"mac join otaa",strlen("mac join otaa"));
#elif JOIN_METHOD == ABP
			printf("mac join abp\n");
			UART_Write(UART1,(unsigned char *)"mac join abp",strlen("mac join abp"));
#endif

#if WAIT_FOR_JOIN == 0

			for(j=0;j<5;j++)
				TIMER_Delay(TIMER1,1000000);

#else	

			if(wait_for_join())
			{
				printf("Joined\n");
			}
			else
			{
				printf("Couldn't join\n");
				timer0_flag = 0;
				continue;
			}
			
#endif			

#if JOIN_METHOD == ABP

				sprintf(transmission_data,"mac tx ucnf %d %04x%02x%02x",LORA_TX_PORT,DEVICE_ADDRESS,'H','i');
				i = 0;
			
#elif JOIN_METHOD == OTAA

				sprintf(transmission_data,"mac tx cnf %d %02x%02x",LORA_TX_PORT,'H','i');
				err_count = 0;
			
#endif 
				do{
					
						printf("%s\n",transmission_data);
						UART_Write(UART1,(unsigned char *)transmission_data,strlen(transmission_data));

#if WAIT_FOR_ACK == 0

						for(j=0;j<5;j++)
						TIMER_Delay(TIMER1,1000000);
					
					}while(++i<5);
#else	

			}while(!check_if_acked()  && ++err_count < 20);
			
#endif

#if JOIN_METHOD == OTAA

			if(err_count == 20)
			{
				printf("Couldn't acknowledge the packet\n");
			}
			else
				printf("Acknowledged the packet successfully... Attempt %d\n",err_count);

#elif JOIN_METHOD == ABP

			printf("Packet has been sent over LoRa with ABP without any ack\n");
			
#endif
			
			PA3=1;	
			printf("Turned off LED\n");
			timer0_flag = 0;
		}	//if(tap/timer status)
		
	}	//while(1)

}	//main
